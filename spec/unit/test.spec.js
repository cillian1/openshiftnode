const chai = require('chai');
 const expect = chai.expect;
 const assert = chai.assert;
 const express = require('express'),
  app = express(),
  bodyParser = require('body-parser'),
  port = process.env.PORT || 8081,
  chaiHttp = require('chai-http');

 // This comment is testing for Jenkinssd
 const Cryptr = require('cryptr');
 const cryptr = new Cryptr('myTotalySecretKey');
 const mysql = require('mysql');
 var server = require('../../app.js');
 app.use(express.static('public'));
 chai.use(chaiHttp);
 const should = chai.should();

// connection configurations
 var mc = mysql.createConnection({
    host     : 'mydbinstance.cwbwsphp0mll.eu-west-1.rds.amazonaws.com',
    user     : 'V1project',
    password : 'V1project',
    database : 'V1project'
 });

  // Test for adding a user in the database and generating the userid
   describe('Registration', () => {
     it('it should register a user', (done) => {
          let contact = {
              fname: "Test",
              lname: "user",
              email:"testuser@gmail.com",
              password:"TestUser123456",
              cpassword:"TestUser123456",
              ph_no:"899625024",
              SQID:"1",
              SQanswer:"dog"
         }
          chai.request(server)
            .post('/registration')
            .send(contact)
            .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('object');
                  res.body.should.have.property('success').eql('user registered sucessfully');
             done();
            });
      });
  });

  // Test to update First Name, Last name & phone number using userid
  describe('Update a user', () => {
      it('Upate test', (done) => {
        let info = {
            userid: "1",
            fname: "Aayush",
            lname: "Jain",
            ph_no:"089 923 423"
        }
          chai.request(server)
            .put('/updateUser')
            .send(info)
            .end((err, res) => {
                  res.should.have.status(200);
                  res.should.be.json;
                  res.body.should.be.a('object');
                  res.body.should.have.property('success').eql('user updated sucessfully');
                //  res.body.should.have.property('fname').eql('Chennian');
              done();
            });
      });
  });

  // Test for validating user credentials before alowing to change password
  describe('Check for Updating the password', () => {
      it('should validate the data', (done) => {
        let info = {
            email:"jignesh.lad@version1.com",
            SQID:"1",
            SQanswer:"version",
        }
          chai.request(server)
            .post('/forgotPass')
            .send(info)
            .end((err, res) => {
                  res.should.have.status(200);
                  res.should.be.json;
                  res.body.should.be.a('object');
                  res.body.should.have.property('success').eql('Security question and answer correct');
                //  res.body.should.have.property('fname').eql('Chennian');
              done();
            });
      });
  });

  //Test for updating the password
  describe('Update the password', () => {
      it('should change the password', (done) => {
        let info = {
            userid: "1",
            password: "Test1234",
            cpassword: "Test1234",
        }
          chai.request(server)
            .put('/updatePass')
            .send(info)
            .end((err, res) => {
                  res.should.have.status(200);
                  res.should.be.json;
                  res.body.should.be.a('object');
                  res.body.should.have.property('success').eql('user password updated sucessfully');
                //  res.body.should.have.property('fname').eql('Chennian');
              done();
            });
      });
  });

 // Test for deleting the user from the database
  describe('Delete Profile Test',()=>{
   it('should delete the user', function(done) {
       chai.request(server)
       .delete('/delete/testuser@gmail.com')
       .end(function(error, response){
         response.should.have.status(200);
         response .should.be.json;
         response.body.should.be.a('object');
         response.body.should.have.property('success').eql('user deleted');
       done();
     });
    });
  });

  // Test for getting user details
  describe('Get Profile Test',()=>{
   it('should get the user profile', function(done) {

       chai.request(server)
       .get('/users/1')
       .end(function(error, response){
         response.should.have.status(200);
         response.should.be.json;
         response.body.should.be.a('object');
         response.body.should.have.property('fname').eql('Aayush');
         response.body.should.have.property('lname').eql('Jain');
         response.body.should.have.property('email').eql('ayush.jain@version1.com');
         done();
     });
    });
  });

  // Test for login
  describe('Login Test',()=>{
  it('should login the user', function(done) {
    let info = {
           email:"ayush.jain@version1.com",
           password:"Ayush123"
          }
      chai.request(server)
      .post('/login')
      .send(info)
      .end(function(error, response){
        mc.query("SELECT email,password FROM Users WHERE email = '"+ info.email +"';",function (err, result) {
          if (err){
            done(err);
            return ;
          }
          if(result==null){
            response.should.have.status(200);
            response.should.be.json;
            response.body.should.be.a('object');
            response.body.should.to.include({success:"Email does not exits"});
        }
          else {
          var expectedval = cryptr.decrypt(result[0].password);
          if (expectedval == info.password ){
          response.should.have.status(200);
          response.should.be.json;
          response.body.should.be.a('object');
          response.body.should.to.include({code:200});
        }
        else{
          response.should.have.status(200);
          response.should.be.json;
          response.body.should.be.a('object');
          response.body.should.to.include({code:204});
        }}
        done();
      });
    });
  });
});
