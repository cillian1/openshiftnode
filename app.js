/*
*
* Main server file
* Packages currently used: express, body-parser
* Port currently used : 8081
*
*/
//Importing packages: "express" and "body-parser"
const express = require('express'),
app = express(),
bodyParser = require('body-parser');

app.set('view engine', 'ejs');

//Using express to serve CSS and Javascript files in the "public" folder.
app.use(express.static('public'));
//identifying the port at 8081
var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0'
 
app.listen(server_port, server_ip_address, function () {
  console.log( "Listening on " + server_ip_address + ", port " + server_port )
});

//Express middleware function that allows the client to easily access the webpage,
//to keep browswer from unblocking the transfer of data between different domains
app.use(function(request, response, next) {
	response.header("Access-Control-Allow-Origin", "*");
	response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});

app.use(express.static('public'));

//Listennig to the port we have set

//app.listen(server_port,server_ip_address);
//This is a log to tell us that we have succesfully connected to the server
//console.log('API server started on: ' + port);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//This imports the routes
var routes = require('./api/routes/userRoutes');
routes(app); //Register the route
module.exports = app;
