
// User Controller file

// == Methods in userController file ==

// -- register_a_user

//-- updateUser

// -- login

// -- delete

// -- Get_Profile

// -- updatePassword


//Acquiring  the "userModel.js" file to use in the Controller file
var userM = require('../models/userModel.js');


// Function to validate an email address.

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}


// "register_a_user" controller calling "register" function from userModel


exports.register_a_user = function(req, res) {
    var Vemail = validateEmail(req.body.email);
    console.log(Vemail);

          //* Checks if all the fields are filled and also validates the user inputs.

          if((req.body.fname == null || (req.body.fname).length > 25)|| (req.body.lname == null || (req.body.lname).length >25) || (req.body.SQanswer == null || (req.body.SQanswer).length > 50) || (req.body.email == null || (req.body.email).length > 50) || (req.body.password == null || (req.body.password).length > 50) || (req.body.cpassword == null || (req.body.cpassword).length > 50)){
            console.log(res);
            console.log('Invalid registration details!!');
            res.json({
              code: 400,
              success: 'Invalid registration details!!'
            });
            }

            //Checking if the password and the confirm pssword are the exact same

            else if(req.body.password != req.body.cpassword){
              console.log(res);
              console.log('Password and Confirmation Password are not the same!!');
              res.json({
                code: 400,
                success: 'Password and Confirmation Password are not the same!!'
              });
            }

            //Checking if the password is at least 8 characters long

            else if( req.body.password.length < 8){
              console.log(res);
              console.log('Password is not long enough!!');
              res.json({
                code: 400,
                success: 'Password is not long enough!!'
              });
            }

            //Checking if the email inputted contains an '@' character

            else if(req.body.email.includes("@")!=true){
              console.log(res);
              console.log('Please enter an @ in the email!!');
              res.json({
                code: 400,
                success: 'Please enter an @ in the email!!'
              });
            }

            //Checks if the inpuuted email contains a '.' 1 position after the '@' character

            else if(req.body.email.includes(".",(req.body.email.indexOf("@")+3)) !=true ){
              console.log(res);
              console.log('Please enter a period in the email!!');
              res.json({
                code: 400,
                success: 'Please enter a period in the email!!'
              });
            }

            //Else continue passing on the sql result
            else{
                  userM.register(req.body, function(result, err) {
                		//If "result" is true, pass the json data, that will be used in "userRoutes.js", else pass the error
                    if (result){
                      console.log(result);
                      console.log('result is true, so passing on JSON data');
                      //If "result" is true, pass the json data, that will be used in "userRoutes.js", else pass the error code
                      res.json(result);
                    } else{
                      //Logging the error
                      console.error("Error: " + err);
                    
                      res.json({
                        code: 400,
                        success: 'Error registering!!'
                      });
                    }
                  }); //End of register method
                }
            };



// "updateUser" controller calling "update" function from userModel

exports.updateUser = function(req, res) {

  //Checks to validate the user inputs before updating their profile

  if((req.body.fname == null || (req.body.fname).length > 25)|| (req.body.lname == null || (req.body.lname).length >25) ){
    console.log(res);
    console.log('Invalid details!!');
    res.json({
      code: 400,
      success: 'Invalid details!!'
    });
    }
  else{
    	userM.update(req.body, function(err, user) {
        if (err){
          //Logging the error
          console.error("Error: " + err);

          res.send(err);
        }
      	// Pass the json data, that will be used in "userRoutes.js", else pass the error code
        console.log(res);
        console.log('JSON data being passed');
        res.json(user);
      });
  }
};


// "login" controller calling "login_user" function from userModel


exports.login = function(req,res){
//Checks to validate the user inputs during the login
    if((req.body.password == null || (req.body.password).length > 50)){
      console.log(res);
      console.log('Invalid details!!');
      res.json({
        code: 400,
        success: 'Invalid details!!'
      });
    }
    //This if statement checks the password is at least 8 characters
    else if( req.body.password.length < 8){
      console.log(res);
      console.log('Password is not long enough!!');
      res.json({
        code: 400,
        success: 'Password is not long enough!!'
      });
    }
    //This if statement checks that the user has inputted a valid email containing an '@'
    else if(req.body.email.includes("@")!=true){
      console.log(res);
      console.log('Please enter an @ in the email!!');
      res.json({
        code: 400,
        success: 'Please enter an @ in the email!!'
      });
    }
    //This if statement checks that the user has inputted a valid email
    //containing a '.' after the '@'
    else if(req.body.email.includes(".",(req.body.email.indexOf("@")+3)) !=true ){
      console.log(res);
      console.log('Please enter a period in the email!!');
      res.json({
        code: 400,
        success: 'Please enter a period in the email!!'
      });
    }
    //Else continue passing on the sql result
    else{
        //  Calling login_user method from model
        userM.login_user(req.body, function(result,err) {
          if (result){
            console.log(result);
            console.log('Passing JSON data');
    				//If "result" is true, pass the json data, that will be used in
            // "userRoutes.js", else pass the error code
            res.json(result);
            } else{
              //Logging the error
              console.error("Error: " + err);

              res.json({
                code: 400,
                success: 'Error logging in!!'
              });
            }
          }); //End of login_user method
        }
      }



// "forgotPass" controller calling "verify_user" function from userModel

exports.forgotPass = function(req,res){


//This if statement checks the seurity answer field is not empty
  if((req.body.SQanswer == null || (req.body.SQanswer).length > 50)){
    console.log(res);
    console.log('Security Field empty!!');
    res.json({
      code: 400,
      success: 'Security Field empty!!'
    });
    }
    //This if statement checks that the user has inputted a valid email containing an '@'
  else if(req.body.email.includes("@")!=true){
      console.log(res);
      console.log('Please enter an @ in the email!!');
      res.json({
        code: 400,
        success: 'Please enter an @ in the email!!'
      });
    }
    //This if statement checks that the user has inputted a valid email containing a '.' after the '@'
    else if(req.body.email.includes(".",(req.body.email.indexOf("@")+3)) !=true ){
      console.log(res);
      console.log('Please enter a period in the email!!');
      res.json({
        code: 400,
        success: 'Please enter a period in the email!!'
      });
    }

    //Else continue passing on the sql result

    else{
    // Calling login_user method from model
        userM.verify_user(req.body, function(result,err) {

          if (result){
            console.log(result);
            console.log('Passing JSON data');
              //If "result" is true, pass the json data, that will be used in "userRoutes.js", else pass the error code
              JSON.stringify(result);
              console.log(result.code);
              if(result.code == 200){
                console.log(result.userid);
                res.render('changeP.ejs',{userid:result.userid});
              }else{
                res.json(result);
              }

              
            } else{
              //Logging the error
              console.error("Error: " + err);

              res.json({
                code: 400,
                success: 'Error logging in!!'
              });
            }
        }); //End of login_user method
      }
}




// "delete" controller calling "remove" function from userModel

exports.delete = function(req,res){
  // Calling remove method from model
  userM.remove(req.params.email,function(result){
		//If "result" is true, pass the json data, that will be used
    // in "userRoutes.js", else pass the error code
        if (result){
          console.log(result);
          console.log('Passing JSON data');
          res.json(result);
        } else{
          //Logging the error
          console.error("Error: " + err);

          res.json({
            code: 400,
            success: 'Error deleting user!!'
          });
        }
    });
  }


// "Get_Profile" controller calling "getProfile" function from userModel

exports.Get_Profile = function(req,res){
    userM.getProfile(req.params.userid,function(err,user){
        if(err){
            //Logging the error
            console.error("Error: " + err);

            res.send(err);
          }
          //Passing on JSON data
        console.log(res);
        console.log('Passing JSON data');
        res.json(user);
    });
};


// "updatePassword" controller calling "updatePass" function from userModel

exports.updatePassword = function(req, res){
    	userM.updatePass(req.body, function(err, user) {
        if (err){
          //Logging the error
          console.error("Error: " + err);

          res.send(err);
        }
      	// Pass the json data, that will be used in "userRoutes.js", else pass the error code
        console.log(res);
        console.log('JSON data being passed');
        res.json(user);
      });
  }
